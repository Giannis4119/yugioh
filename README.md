# Project Yu-Gi-Oh!

This project lists all the available cards along with their details. All cards retrieved from YugiohPrices.com API

## Requirements

For development, you will only need Node.js installed on your environement.

### Node

[Node](http://nodejs.org/) is really easy to install & now include [NPM](https://npmjs.org/).
You should be able to run the following command after the installation procedure
below.

    $ node --version
    v8.11.3

    $ npm --version
    5.6.0

#### Node installation on Linux

    sudo apt-get install python-software-properties
    sudo add-apt-repository ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install nodejs

#### Node installation on Windows

Just go on [official Node.js website](http://nodejs.org/) & grab the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://Giannis4119@bitbucket.org/Giannis4119/yugioh.git
    $ cd yugioh
    $ npm install

## Start & watch

    $ npm start

## Simple build for production

    $ npm run build

---

## Languages & tools


### JavaScript

- [React](http://facebook.github.io/react) is used for UI.

### CSS

- [material-ui](https://material-ui.com/) is used to write css style to the project).

### Testing tools

 - [Enzyme](https://airbnb.io/enzyme/) is used for unit testing

---

## Tests

Basic unit tests implemented

### Run all tests

$ npm run test