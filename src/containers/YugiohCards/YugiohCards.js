import React, { Component } from 'react';
import axios from 'axios';
import CardsList from '../../components/CardsList/CardsList';
import Grid from '@material-ui/core/Grid';
import CardDetails from '../../components/CardDetails/CardDetails';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Spinner from '../../components/UI/Spinner/Spinner';
import Modal from '../../components/UI/Modal/Modal';

const cardsList= [
    'Burial from a Different Dimension',
    'Charge of the Light Brigade',
    'Infernoid Antra',
    'Infernoid Attondel',
    'Infernoid Decatron',
    'Infernoid Devyaty',
    'Infernoid Harmadik',
    'Infernoid Onuncu',
    'Infernoid Patrulea',
    'Infernoid Pirmais',
    'Infernoid Seitsemas',
    'Lyla, Lightsworn Sorceress',
    'Monster Gate',
    'One for One',
    'Raiden, Hand of the Lightsworn',
    'Reasoning',
    'Time-Space Trap Hole',
    'Torrential Tribute',
    'Upstart Goblin',
    'Void Seer'
]

class YugiohCards extends Component {
    state = {
        cards: [],
        loading: true,
        selectedCard: null,
        errorLoading: false,
        error: null
    }

    

    componentWillMount() {
        this.getCards();
    }

    getCards = () => {
        this.setState({
            loading: true,
            selectedCard: null,
            errorLoading: false,
            error: null
        })
        axios.all(cardsList.map(l => axios.get('http://52.57.88.137/api/card_data/'+l)))
            .then(axios.spread((...res) => {
                this.setState({
                    cards: res,
                    loading: false
                })
            })).catch( error => {     
                this.setState({ 
                    loading: false,
                    cards: [] ,
                    errorLoading: true,
                    error: error
                });
            });
    }

    cardListClickHandler = (card) => {
        this.setState({
            selectedCard: {...card}
        })
    }

    errorConfirmedHandler = () => {
        this.setState({
            errorLoading: null,
            error: null
        })
    }

    render() {

        return (
            <>
                <Modal show={this.state.error}
                    modalClosed={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                </Modal>
                <Grid container spacing={40} >
                    <Grid item xs={3}>
                        <Drawer
                            variant="persistent"
                            anchor="left"
                            open
                        >
                            {
                                !this.state.loading?
                                <CardsList 
                                    getCards={this.getCards}
                                    cards={this.state.cards}
                                    loading={this.state.loading}
                                    cardListClickHandler={this.cardListClickHandler}
                                    error={this.state.error}
                                /> : <Spinner />
                            }
                        </Drawer>
                    </Grid>
                    <Divider />
                    <Grid item xs={9}>
                        <CardDetails 
                            selectedCard={this.state.selectedCard}
                        />
                    </Grid>
                </Grid>
                             
            </>
        )
    }
}

export default YugiohCards;//,axios);