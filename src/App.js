import React, { Component } from 'react';
import YugiohCards from './containers/YugiohCards/YugiohCards';

class App extends Component {
  render() {
    return (
      <YugiohCards />
    );
  }
}

export default App;
