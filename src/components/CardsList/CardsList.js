import React from 'react';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Refresh from '@material-ui/icons/Refresh';
import IconButton from '@material-ui/core/IconButton';

const cardsList = (props) => {
    return (     
            <>
                <div>
                    <IconButton style={{float: "right"}}>
                        <Refresh title="Refresh" onClick={props.getCards}/>
                    </IconButton>
                </div>
                <Divider />
                <List>
                    {
                        props.cards &&
                        props.cards.map((card, index) => {
                            return (
                                <div key={index}>
                                    <ListItem button onClick={() => props.cardListClickHandler(card.data.data)}>
                                        <ListItemIcon>
                                            <Avatar>
                                                <InboxIcon />
                                            </Avatar>
                                        </ListItemIcon>
                                        <ListItemText primary={card.data.data.name} secondary={card.data.data.card_type} button />
                                        <Divider />
                                    </ListItem>
                                    <Divider />
                                </div>
                                )
                            })
                    }
                </List>                
            </>
    )
}

export default cardsList;