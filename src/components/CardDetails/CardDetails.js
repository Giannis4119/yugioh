import React from 'react';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import ButtonBase from '@material-ui/core/ButtonBase';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import StarSharp from '@material-ui/icons/StarSharp';

const cardDetails = (props) => {
            return (
                <div>
                    {
                        !props.selectedCard ? 
                        <Toolbar>
                            <Typography variant="h6" color="inherit" noWrap>
                                Click a card and let the game begin!
                            </Typography>
                        </Toolbar>
                        : 
                        <div>
                            <Paper>
                                <Grid container spacing={16}>
                                    <Grid item>
                                        <ButtonBase>
                                        <img src={'http://52.57.88.137/api/card_image/'+props.selectedCard.name} />
                                        </ButtonBase>
                                    </Grid>
                                    <Grid item xs={12} sm container>
                                        <Grid item xs container direction="column" spacing={16}>
                                            <Grid item xs>
                                                <Typography gutterBottom variant="h2">
                                                    {props.selectedCard.name}
                                                </Typography>
                                                { 
                                                    props.selectedCard.level &&
                                                    Array(props.selectedCard.level).fill().map((_, i) => <StarSharp key={i}/>)
                                                }
                                                {
                                                    props.selectedCard.family &&
                                                        <span style={{display: 'block'}}>This is a {props.selectedCard.family} card</span>
                                                }
                                                <Card>
                                                    <CardContent>
                                                        <Typography component="title">Card Details</Typography>
                                                        {
                                                            props.selectedCard.atk ?
                                                            <Typography color="textSecondary" component="h2"  gutterBottom>
                                                                <span>Atk/</span> {props.selectedCard.atk}
                                                            </Typography> : 
                                                            <Typography color="textSecondary" component="h2"  gutterBottom>
                                                                <span></span>
                                                            </Typography> 
                                                        }
                                                        {
                                                            props.selectedCard.def ?
                                                            <Typography color="textSecondary" gutterBottom>
                                                                <span>Def/</span> {props.selectedCard.def}
                                                            </Typography> : 
                                                            <Typography color="textSecondary" gutterBottom>
                                                                <span></span> 
                                                            </Typography>  
                                                        }
                                                        <Typography gutterBottom component="paragraph">
                                                            {
                                                                props.selectedCard.type &&
                                                                <><strong>[{props.selectedCard.type}]</strong><br/></>
                                                            }                                                            
                                                            {props.selectedCard.text}
                                                        </Typography>
                                                    </CardContent>
                                                </Card>                                                
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </div>
                    }
                             
                </div>
          )
      }

export default cardDetails;

{/*
 <Typography paragraph>
                        {
                            props.selectedCard &&
                            <>
                                <p align="center">
                                    <h1>{props.selectedCard && props.selectedCard.name}</h1>
                                </p>
                                <p>
                                    <img style={{"float": "left", "marginRight": "15px"}} src={'http://52.57.88.137/api/card_image/'+props.selectedCard.name} />
                                    <h2>Card Details</h2>
                                    {props.selectedCard.text}
                                </p>
                            </>
                        }          
                    </Typography>      
*/}